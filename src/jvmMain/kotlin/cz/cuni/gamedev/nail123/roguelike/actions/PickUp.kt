package cz.cuni.gamedev.nail123.roguelike.actions

import cz.cuni.gamedev.nail123.roguelike.entities.items.HealthPack
import cz.cuni.gamedev.nail123.roguelike.entities.items.Item
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import cz.cuni.gamedev.nail123.roguelike.world.Area

class PickUp: GameAction() {
    override fun tryPerform(area: Area): Boolean {
        val playerPos = area.player.position
        var item = area[playerPos]!!.entities.filterIsInstance<Item>().firstOrNull()
        if (item == null) {
            logMessage("There is nothing to pick up here.")
            return false
        }

        // We directly try to equip - we don't need to deal with inventory management immediately
        if(item is HealthPack){ // Try equip healthpack
            //Uncomment this if you don't want healthpacks to be wasted
            /*
            if(area.player.hitpoints == area.player.maxHitpoints){
                return true;
            }
            */
            area.player.hitpoints += item.health
            if(area.player.hitpoints > area.player.maxHitpoints)
                area.player.hitpoints = area.player.maxHitpoints
            item.block.entities.remove(item);
            return true
        }
        return if (item.isEquipable(area.player).success) {
            area.player.tryEquip(item).success
        } else {
            area.player.tryPickUp(item)
            true
        }
    }
}