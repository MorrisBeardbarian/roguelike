package cz.cuni.gamedev.nail123.roguelike.entities.items

import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class Bow(val attackPower: Int, val attackRange: Int): Weapon(GameTiles.BOW) {
    override fun onEquip(character: HasInventory) {
        if (character is Player) {
            character.rangeAttackDamage += attackPower
            character.attackRange += attackRange
        }
    }

    override fun onUnequip(character: HasInventory) {
        if (character is Player) {
            character.rangeAttackDamage -= attackPower
            character.attackRange -= attackPower
        }
    }

    override fun toString(): String {
        return "Bow($attackPower, $attackRange)"
    }
}