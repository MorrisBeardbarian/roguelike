package cz.cuni.gamedev.nail123.roguelike.entities.attributes

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.MovingEntity
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Enemy
import cz.cuni.gamedev.nail123.roguelike.extensions.shift
import cz.cuni.gamedev.nail123.roguelike.world.Direction
import org.hexworks.zircon.api.data.Position3D

interface HasRangedAttack {
    var attackRange: Int
    var rangeAttackDamage: Int

    fun shoot(direction: Direction) {
        var i = 0
        val gameArea = (this as GameEntity?)?.area
        val curPosition = this.position
        var nextBlock = gameArea?.get(curPosition)
        if (gameArea != null && nextBlock != null) {
            while (i < attackRange) {
                if(nextBlock != null) {
                    nextBlock = gameArea.get(nextBlock.position shift direction)
                }
                if (nextBlock != null) {
                    for (entity in nextBlock.entities) {
                        if (entity is Enemy) {
                            interaction(this, nextBlock, InteractionType.RANGED)
                        }
                        break;
                    }
                }
                i++;
            }
        }
    }
}