package cz.cuni.gamedev.nail123.roguelike.entities.enemies

import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasSmell
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword
import cz.cuni.gamedev.nail123.roguelike.entities.items.Bow
import cz.cuni.gamedev.nail123.roguelike.entities.items.HealthPack
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.mechanics.goBlindlyTowards
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import kotlin.math.ceil
import kotlin.math.roundToInt
import kotlin.random.Random

class Rat: Enemy(GameTiles.RAT), HasSmell {
    override val blocksMovement = true
    override val blocksVision = false
    override val smellingRadius = 7

    override val maxHitpoints = 6 + ceil((6*powerEnhancer).toDouble()).roundToInt()
    override var hitpoints = 6 + ceil((6*powerEnhancer).toDouble()).roundToInt()
    override var attack = 2 + ceil((2*powerEnhancer).toDouble()).roundToInt()
    override var defense = 0

    override fun update() {
        if (Pathfinding.chebyshev(position, area.player.position) <= smellingRadius) {
            goBlindlyTowards(area.player.position)
        }
    }

    override fun die() {
        super.die()
        // Drop a sword
        var random = (0..2).random()
        if(random == 0) {
            var lowerLimit = 3 + ceil((3*itemDropEnhancer).toDouble()).roundToInt()
            var upperLimit = 5 + ceil((5*itemDropEnhancer).toDouble()).roundToInt()
            val swordPower = Random.nextInt(upperLimit - lowerLimit) + lowerLimit //power is random, but is limited to smaller amounts
            this.block.entities.add(Sword(swordPower))
        }else if(random == 1){
            var lowerLimit = 1 + ceil((1*itemDropEnhancer).toDouble()).roundToInt()
            var upperLimit = 3 + ceil((3*itemDropEnhancer).toDouble()).roundToInt()
            val bowPower = Random.nextInt(upperLimit - lowerLimit) + lowerLimit //power is random, but is limited to smaller amounts
            val bowRange = (3..5).random()
            this.block.entities.add(Bow(bowPower, bowRange))
        }else{
            this.block.entities.add(HealthPack(4))
        }
    }
}