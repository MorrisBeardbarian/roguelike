package cz.cuni.gamedev.nail123.roguelike.actions

import cz.cuni.gamedev.nail123.roguelike.world.Direction
import cz.cuni.gamedev.nail123.roguelike.world.Area

data class Shoot(val direction: Direction): GameAction() {
    override fun tryPerform(area: Area): Boolean {
        area.player.shoot(direction)
        return true
    }
}