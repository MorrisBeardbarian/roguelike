package cz.cuni.gamedev.nail123.roguelike.entities.items

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.*
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.zircon.api.data.Tile

class HealthPack(val health: Int): Item(GameTiles.HEALTH), Interactable {
    override val blocksMovement = false
    override val blocksVision = false

    override fun isEquipable(character: HasInventory): Inventory.EquipResult{
        return Inventory.EquipResult.Success
    }

    override fun onEquip(inventory: Inventory, character: HasInventory) {
        return
    }
    override fun onUnequip(inventory: Inventory, character: HasInventory) {
        return
    }

    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type) {
        withEntity<Player>(type) { logMessage("Here lies ${this@HealthPack}") }
    }

    override fun onEquip(character: HasInventory) {
        return
    }

    override fun onUnequip(character: HasInventory) {
        return
    }
}
