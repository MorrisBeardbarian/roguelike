package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import cz.cuni.gamedev.nail123.roguelike.entities.objects.Stairs;
import cz.cuni.gamedev.nail123.roguelike.events.LoggedEvent;
import cz.cuni.gamedev.nail123.roguelike.world.Area;
import cz.cuni.gamedev.nail123.roguelike.world.World;
import cz.cuni.gamedev.nail123.roguelike.world.builders.AreaBuilder;
import cz.cuni.gamedev.nail123.roguelike.world.builders.EmptyAreaBuilder;
import cz.cuni.gamedev.nail123.roguelike.world.worlds.BinaryDivision.BinaryDivisionAreaBuilder;
import org.hexworks.zircon.api.data.Position3D;
import org.hexworks.zircon.api.data.Size3D;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class SampleJavaWorld extends World {

    /**
     * Seed for generator
     */
    static private long seed = 19960709L;

    /**
     * Generator for the seeds of the levels
     */
    static private Random levelsRNG = new Random(seed);


    static private HashMap<Integer, Long> levelsSeeds = new HashMap<Integer, Long>();

    static private HashMap<Integer, Boolean> levelsBossRoom = new HashMap<Integer, Boolean>();

    int currentLevel = 0;

    BinaryDivisionAreaBuilder areaBuilder;

    public SampleJavaWorld() {
        levelsRNG.setSeed(seed);
        //if(!levelsSeeds.containsKey(currentLevel)) {
        //    levelsSeeds.put(currentLevel, levelsRNG.nextLong());
        //}
    }

    @NotNull
    @Override
    public Area buildStartingArea() {
        return buildLevel();
    }

    Area buildLevel() {
        if(currentLevel == 0)
            levelsRNG.setSeed(seed);
        if(!levelsSeeds.containsKey(currentLevel)){
            levelsSeeds.put(currentLevel, levelsRNG.nextLong());
        }
        // Start with an empty area
        if(areaBuilder == null)
            areaBuilder = (new BinaryDivisionAreaBuilder());
        areaBuilder.create(levelsSeeds.get(currentLevel));
        areaBuilder.placePlayer();
        areaBuilder.placeEnd();

        // Build it into a full Area
        return areaBuilder.build();
    }

    /**
     * Moving down - goes to a brand new level.
     */
    @Override
    public void moveDown() {
        ++currentLevel;
        (new LoggedEvent(this, "Descended to level " + (currentLevel + 1))).emit();
        areaBuilder.enhancer += 0.1; // increase difficulty
        areaBuilder.getPlayer().setMaxHitpoints(areaBuilder.getPlayer().getMaxHitpoints() + new Double(Math.ceil(areaBuilder.getPlayer().getMaxHitpoints() * areaBuilder.enhancer)).intValue() );
        if (currentLevel >= getAreas().getSize()) getAreas().add(buildLevel());
        goToArea(getAreas().get(currentLevel));
    }

    /**
     * Moving up would be for revisiting past levels, we do not need that. Check [DungeonWorld] for an implementation.
     */
    @Override
    public void moveUp() {
        // Not implemented
    }
}
