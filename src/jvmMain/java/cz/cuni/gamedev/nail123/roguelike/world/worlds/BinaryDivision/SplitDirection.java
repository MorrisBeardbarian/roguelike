package cz.cuni.gamedev.nail123.roguelike.world.worlds.BinaryDivision;

public enum SplitDirection {
    VERTICAL,
    HORIZONTAL;
}
