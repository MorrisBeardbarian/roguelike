package cz.cuni.gamedev.nail123.roguelike.world.worlds.BinaryDivision;

import cz.cuni.gamedev.nail123.roguelike.GameConfig;
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Stairs;
import cz.cuni.gamedev.nail123.roguelike.world.builders.AreaBuilder;
import org.hexworks.zircon.api.data.Position3D;
import org.hexworks.zircon.api.data.Size3D;
import org.jetbrains.annotations.NotNull;

public class BinaryDivisionAreaBuilder extends AreaBuilder {

    public double enhancer = 0.0;

    public static int levelWidth = 50;
    public static int levelHeight = 50;

    LevelArea level = null;
    Size3D ourSize;

    /**
     * Should connect rooms when we do the split
     */
    public static boolean BINARY_CONNECTIONS = true;

    /**
     * Should carve rooms smaller than they actually are
     */
    public static boolean CARVED_ROOMS = false;

    /**
     * Connects all neighbouring rooms
     */
    public static boolean CONNECT_ALL_NEIGHBOURS = false;

    public BinaryDivisionAreaBuilder(){
        super(GameConfig.getAreaSize(), GameConfig.getVisibleSize());
        ourSize = GameConfig.getAreaSize();
    }

    /**
     * Creates the level
     * @return
     */
    @NotNull
    public BinaryDivisionAreaBuilder create() {
        try {
            level.generateRooms();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        try{
            level.carveRooms();
            level.setRoomNeighbours();
            level.connectRooms();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        level.fixRoomsUnreachable();
        try{
            level.renderRooms(this.getBlocks());
            level.renderTunnels(this.getBlocks());
        }catch(Exception ex){
            ex.printStackTrace();
        }

        level.placeEnemies(this, this.getBlocks());

        level.printStats();

        return this;
    }

    public BinaryDivisionAreaBuilder create(Long seed){
        level = new LevelArea(seed);
        level.setSize(this.getWidth(), this.getHeight());
        return this.create();
    }

    @NotNull
    @Override
    public Size3D getSize() {
        return this.ourSize;
    }

    public void placePlayer(){
        Position3D pos = this.level.getStart().getRandomPosition();
        this.addEntity(this.getPlayer(), pos);
    }

    public void placeEnd(){
        Position3D pos = this.level.getEnd().getRandomPosition();
        this.addEntity(new Stairs(), pos);
    }
}
