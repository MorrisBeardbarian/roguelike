package cz.cuni.gamedev.nail123.roguelike.world.worlds.BinaryDivision;

import cz.cuni.gamedev.nail123.roguelike.blocks.Floor;
import cz.cuni.gamedev.nail123.roguelike.blocks.GameBlock;
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Rat;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Boss;
import org.hexworks.zircon.api.data.Position3D;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Room {
    /**
     * X start coordinate of the room
     */
    public int x;
    /**
     * Y start coordinate of the room
     */
    public int y;
    /**
     * Width of the room
     */
    public int width;
    /**
     * Height of the room
     */
    public int height;

    /**
     * This rooms number
     */
    public int number = 0;

    /**
     * Total number of rooms
     */
    public static int roomNumber = 0;

    /**
     * Is room a start room
     */
    public boolean start = false;
    /**
     * Is room an end room
     */
    public boolean end = false;
    /**
     * Is room a boss room
     */
    public boolean bossRoom = false;

    /**
     * Can the room be split further
     */
    public boolean canSplitFurther = true;

    /**
     * Did the splitting end
     */
    public boolean splitEnded = false;

    /**
     * Set of rooms connected to this room
     */
    public HashSet<Room> connectedRooms = new HashSet<Room>();

    /**
     * List of neighbours adjacent to this room
     */
    public ArrayList<Room> neighbours = new ArrayList<Room>();

    /**
     * Maximum width a room should have
     */
    public static int MAX_WIDTH_THRESHOLD = 30;
    /**
     * Maximum height a room should have
     */
    public static int MAX_HEIGHT_THRESHOLD = 30;

    /**
     * Minimum width a room should have (rooms can be smaller than this, but from this value they will stop being able to be split)
     */
    public static int MIN_WIDTH_THRESHOLD = 10;
    /**
     * Minimum height a room should have (rooms can be smaller than this, but from this value they will stop being able to be split)
     */
    public static int MIN_HEIGHT_THRESHOLD = 10;

    /**
     * X start coordinate of the carved room
     */
    private int startX = 1;

    /**
     * Y start coordinate of the carved room
     */
    private int startY = 1;

    /**
     * Width of the carved room
     */
    private int carvedWidth = 5;

    /**
     * Height of the carved room
     */
    private int carvedHeight = 5;

    /**
     * Holds the room randomness
     */
    private Random roomRNG = null;

    /**
     * Has the room been rendered
     */
    private boolean rendered = false;

    /**
     * Constructor
     * @param x
     * @param y
     * @param width
     * @param height
     * @param canSplitFurther
     */
    public Room(int x, int y, int width, int height, boolean canSplitFurther){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.canSplitFurther = canSplitFurther;
        roomNumber++;
        this.number = roomNumber;
    }

    /**
     * Constructor
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public Room(int x, int y, int width, int height){
        this(x, y, width, height, true);
    }

    /**
     * Set this room as a START room
     * @param isStart
     */
    public void setStart(boolean isStart){
        this.start = isStart;
    }

    /**
     * Set this room as an END room
     * @param isEnd
     */
    public void setEnd(boolean isEnd){
        this.end = isEnd;
    }

    /**
     * Set this room as a BOSS room
     * @param isBossRoom
     */
    public void setBossRoom(boolean isBossRoom){
        this.bossRoom = isBossRoom;
    }

    /**
     * Carve the room to be smaller so bigger tunnels between rooms exist
     */
    public void carveRoom(){
        if(BinaryDivisionAreaBuilder.CARVED_ROOMS) {
            int minWidthPercentage = 75;
            int maxWidthPercentage = 100;
            int minHeightPercentage = 75;
            int maxHeightPercentage = 100;
            int widthPercentage = this.roomRNG.nextInt(maxWidthPercentage - minWidthPercentage) + minWidthPercentage;
            int heightPercentage = this.roomRNG.nextInt(maxHeightPercentage - minHeightPercentage) + minHeightPercentage;
            this.carvedWidth = (int) Math.floorDiv(widthPercentage * this.width, 100);
            this.carvedHeight = (int) Math.floorDiv(heightPercentage * this.height, 100);
            this.startX = this.x + (int) Math.floorDiv(this.width - this.carvedWidth, 2);
            this.startY = this.y + (int) Math.floorDiv(this.height - this.carvedHeight, 2);
        }else{
            this.carvedWidth = this.width;
            this.carvedHeight = this.height;
            this.startX = this.x;
            this.startY = this.y;
        }
    }

    /**
     * Randomly splits the room either horizontally or vertically
     * @return
     * @throws Exception
     */
    public Room randomSplit() throws Exception{
        Room new_room = null;
        boolean split = false;
        //Do not try to split further than minimum allowed sizes
        if(
            (this.width / 2 < MIN_WIDTH_THRESHOLD &&
            this.height / 2 < MIN_HEIGHT_THRESHOLD) ||
            (this.width <= MAX_WIDTH_THRESHOLD &&
            this.height <= MAX_HEIGHT_THRESHOLD &&
            this.roomRNG.nextBoolean()) //Do not split any further
        ){
            this.splitEnded = true;
            return null;
        }

        // we can only split horizontally
        if(this.width / 2 < MIN_WIDTH_THRESHOLD && this.height > MAX_HEIGHT_THRESHOLD){
            new_room = this.callSplitByDirection(SplitDirection.HORIZONTAL); // must split
            split = true;
        }else{
            if (this.width / 2 < MIN_WIDTH_THRESHOLD && this.height <= MAX_HEIGHT_THRESHOLD) {
                if (this.roomRNG.nextBoolean()) {
                    new_room = this.callSplitByDirection(SplitDirection.HORIZONTAL);
                    split = true;
                } else {
                    this.splitEnded = true;
                    return null;
                }
            }
        }

        // we can only split vertically
        if(this.height / 2 < MIN_HEIGHT_THRESHOLD && this.width > MAX_WIDTH_THRESHOLD && !split) {
            new_room = this.callSplitByDirection(SplitDirection.VERTICAL); // must split
            split = true;
        }else{
            if (this.height / 2 < MIN_HEIGHT_THRESHOLD && this.width <= MAX_WIDTH_THRESHOLD && !split) {
                if (this.roomRNG.nextBoolean()) {
                    new_room = this.callSplitByDirection(SplitDirection.VERTICAL);
                    split = true;
                } else {
                    this.splitEnded = true;
                    return null;
                }
            }
        }

        // we can split either direction
        if(this.height / 2 >= MIN_HEIGHT_THRESHOLD && this.width / 2 >= MIN_WIDTH_THRESHOLD && !split && !this.splitEnded){
            SplitDirection splitDirection;
            switch (this.roomRNG.nextInt(2)){
                case 0:
                    splitDirection = SplitDirection.HORIZONTAL;
                    break;
                case 1:
                    splitDirection = SplitDirection.VERTICAL;
                    break;
                default:
                    throw new Exception("Split direction unavailable for split.");
            }
            new_room = this.callSplitByDirection(splitDirection);
        }
        // Could move start room to the new room
        if(this.start && new_room != null){
            if(this.roomRNG.nextBoolean()){
                this.start = false;
                new_room.start = true;
            }
        }
        // Could move end room to the new room
        if(this.end && new_room != null){
            if(this.roomRNG.nextBoolean()){
                this.end = false;
                new_room.end = true;
            }
        }
        // Don't allow end and start in the same room
        if(this.end && this.start && new_room != null){
            this.end = false;
            new_room.end = true;
        }
        /*
        if(new_room != null){
            // Set new room's connected rooms and neighbours to the same one as the big room (but to a copy, obviously)
            new_room.connectedRooms = new HashSet<Room>(this.connectedRooms);
            new_room.neighbours = new ArrayList<Room>(this.neighbours);
            // Disconnect the rooms already connected that are no longer adjacent
            this.reconnectRooms();
            new_room.reconnectRooms();
            //Add each other to neighbours
            this.neighbours.add(new_room);
            new_room.neighbours.add(this);
            //Connect each other if allowed
            if(BinaryDivisionAreaBuilder.BINARY_CONNECTIONS) {
                this.connectedRooms.add(new_room);
                new_room.connectedRooms.add(new_room);
            }
        }
        */
        return new_room;
    }

    /**
     * Calls the necessary method for splitting from the specified direction
     * @param dir
     * @return
     * @throws Exception
     */
    private Room callSplitByDirection(@NotNull SplitDirection dir) throws Exception {
        Room tmp = null;
        switch (dir){
            case HORIZONTAL:
                tmp = this.splitHorizontal();
                break;
            case VERTICAL:
                tmp = this.splitVertical();
                break;
            default:
                throw new Exception("Unavailable split direction.");
        }
        return tmp;
    }

    /**
     * Splits the room horizontally
     * @return
     */
    public Room splitHorizontal(){
        //Choose a random split offset
        ArrayList<Integer> split_offsets = new ArrayList<Integer>();
        split_offsets.add(-2);
        split_offsets.add(-1);
        split_offsets.add(0);
        split_offsets.add(1);
        split_offsets.add(2);
        Integer random_split_offset = split_offsets.get(roomRNG.nextInt(split_offsets.size()));
        //Do the split
        int split = Math.floorDiv(this.height, 2) + random_split_offset;
        this.height = this.height - split - 1;
        //Return a new room on the split part
        return new Room(this.x, this.y + this.height + 1, this.width, split);
    }

    /**
     * Splits the room vertically
     * @return
     */
    public Room splitVertical(){
        //Choose a random split offset
        ArrayList<Integer> split_offsets = new ArrayList<Integer>();
        split_offsets.add(-2);
        split_offsets.add(-1);
        split_offsets.add(0);
        split_offsets.add(1);
        split_offsets.add(2);
        Integer random_split_offset = split_offsets.get(roomRNG.nextInt(split_offsets.size()));
        //Do the split
        int split = Math.floorDiv(this.width, 2) + random_split_offset;
        this.width = this.width - split - 1;
        //Return a new room on the split part
        return new Room(this.x + this.width + 1, this.y, split, this.height);
    }

    /**
     * Verifies if neighbour rooms or connected rooms are still adjacent, and if they aren't, the connection or neighbourhood is dropped
     */
    public void reconnectRooms(){
        // Remove each connection between rooms that are no more adjacent
        for (Iterator<Room> iterator = neighbours.iterator(); iterator.hasNext(); ) {
            Room neighbour = iterator.next();
            if (this.isAdjacentTo(neighbour) <= 1) {
                iterator.remove();
                neighbour.neighbours.remove(this);
                this.connectedRooms.remove(neighbour);
                neighbour.connectedRooms.remove(this);
            }
        }
    }

    /**
     * How is this room positioned about the other (to the left=4, to the right=2, to the bottom=3 or to the top=1)
     * Clockwise verification
     * Other room provided must be neighbour
     * Returns 0 if rooms are not neighbours
     * @param other
     * @return
     */
    public void drawTunnelBetween(Room other, Map<Position3D, GameBlock> blocks) throws Exception {
        if(!this.neighbours.contains(other)){
            throw new Exception("These rooms are not neighbours");
        }
        boolean this_above = this.y + this.height - 1 < other.y; //this is above the other
        boolean this_right = this.x > other.x+other.width - 1; //this is to the right of other
        boolean this_below = this.y > other.y + other.height; //this is below the other
        boolean this_left = this.x + this.width < other.x; //this is to the left of other
        ArrayList<Integer> rows = this.rowsForConnection(other);
        ArrayList<Integer> cols = this.colsForConnection(other);
        if(rows.size() < 3 && cols.size() < 3){
            throw new Exception("Something bad happened and we have a neighbour which is not really neighbour");
        }
        if(rows.size() > cols.size()){
            int startTunnelRow = this.startY + rows.get(this.roomRNG.nextInt(rows.size() - 2) + 1);
            int endTunnelRow = startTunnelRow + 1;
            if(this_right){
                int startTunnelCol = other.startX + other.carvedWidth;
                int endTunnelCol = this.startX;
                drawTunnelHorizontally(startTunnelRow, endTunnelRow, startTunnelCol, endTunnelCol, blocks);
            }else{
                int startTunnelCol = this.startX + this.carvedWidth;
                int endTunnelCol = other.startX;
                drawTunnelHorizontally(startTunnelRow, endTunnelRow, startTunnelCol, endTunnelCol, blocks);
            }
        }else{
            int startTunnelCol = this.startX + cols.get(this.roomRNG.nextInt(cols.size() - 2) + 1);
            int endTunnelCol = startTunnelCol + 1;
            if(this_above){
                int startTunnelRow = this.startY + this.carvedHeight;
                int endTunnelRow = other.startY;
                drawTunnelVertically(startTunnelRow, endTunnelRow, startTunnelCol, endTunnelCol, blocks);
            }else{
                int startTunnelRow = other.startY + other.carvedHeight;
                int endTunnelRow = this.startY;
                drawTunnelVertically(startTunnelRow, endTunnelRow, startTunnelCol, endTunnelCol, blocks);
            }
        }
    }

    /**
     * Draws a 1 floor tunnel vertically
     * @param startTunnelRow
     * @param endTunnelRow
     * @param startTunnelCol
     * @param endTunnelCol
     * @param blocks
     */
    private void drawTunnelVertically(int startTunnelRow, int endTunnelRow, int startTunnelCol, int endTunnelCol, Map<Position3D, GameBlock> blocks){
        //System.out.println("Drawing tunnel vertically from "+startTunnelCol + " and "+startTunnelRow + " to " + endTunnelCol + " and "+ endTunnelRow);
        for(int j = startTunnelRow; j <= endTunnelRow; j++){
            //PRO GAMER MOVE
            Position3D pos1 = Position3D.create(endTunnelCol - 2, j, 0); //First tunnel wall
            Position3D pos2 = Position3D.create(endTunnelCol, j, 0); //Second tunnel wall
            Position3D pos3 = Position3D.create(endTunnelCol - 1, j, 0); //The tunnel floor
            if(blocks.containsKey(pos1)){
                blocks.remove(pos1);
            }
            blocks.put(pos1, new Wall());
            if(blocks.containsKey(pos2)){
                blocks.remove(pos2);
            }
            blocks.put(pos2, new Wall());
            if(blocks.containsKey(pos3)){
                blocks.remove(pos3);
            }
            blocks.put(pos3, new Floor());
        }
    }

    /**
     * Draws a 1 floor tunnel horizontally
     * @param startTunnelRow
     * @param endTunnelRow
     * @param startTunnelCol
     * @param endTunnelCol
     * @param blocks
     */
    private void drawTunnelHorizontally(int startTunnelRow, int endTunnelRow, int startTunnelCol, int endTunnelCol, Map<Position3D, GameBlock> blocks){
        //System.out.println("Drawing tunnel horizontally from "+startTunnelCol + " and "+startTunnelRow + " to " + endTunnelCol + " and "+ endTunnelRow);
        for(int j = startTunnelCol; j <= endTunnelCol; j++){
            //PRO GAMER MOVE
            Position3D pos1 = Position3D.create(j, endTunnelRow - 2, 0); //First tunnel wall
            Position3D pos2 = Position3D.create(j, endTunnelRow, 0); //Second tunnel wall
            Position3D pos3 = Position3D.create(j, endTunnelRow - 1, 0); //The tunnel floor
            if(blocks.containsKey(pos1)){
                blocks.remove(pos1);
            }
            blocks.put(pos1, new Wall());
            if(blocks.containsKey(pos2)){
                blocks.remove(pos2);
            }
            blocks.put(pos2, new Wall());
            if(blocks.containsKey(pos3)){
                blocks.remove(pos3);
            }
            blocks.put(pos3, new Floor());
        }
    }

    /**
     * Returns all rows which are in both this and other room
     * @param other
     * @return
     */
    private ArrayList<Integer> rowsForConnection(Room other){
        ArrayList<Integer> ret = new ArrayList<Integer>();
        for(int i=0; i<=this.carvedHeight; i++){
            if(this.startY + i >= other.startY && this.startY + i <= other.startY + other.carvedHeight){
                ret.add(i);
            }
        }
        return ret;
    }

    /**
     * Returns all columns which are in both this and other room
     * @param other
     * @return
     */
    private ArrayList<Integer> colsForConnection(Room other){
        ArrayList<Integer> ret = new ArrayList<Integer>();
        for(int i=0; i<=this.carvedWidth; i++){
            if(this.startX + i >= other.startX && this.startX + i <= other.startX + other.carvedWidth){
                ret.add(i);
            }
        }
        return ret;
    }

    /**
     * Returns in how many points the rooms are adjacent by
     * @param other
     * @return
     */
    public int isAdjacentTo(Room other){

        int col1 = this.x;
        int col2 = this.x + this.width;
        int col3 = other.x;
        int col4 = other.x + other.width;
        int row1 = this.y;
        int row2 = this.y + this.height;
        int row3 = other.y;
        int row4 = other.y + other.height;

        ArrayList<Integer> rows = this.rowsForConnection(other);
        ArrayList<Integer> cols = this.colsForConnection(other);
        if(rows.size() < 3 && cols.size() < 3){
            return 0; // rooms not adjacent enough
        }

        int colsAdjacency = 0;
        if( Math.abs(row1 - row4) == 1 || Math.abs(row2 - row3) == 1) {
            for (int i = col1; i < col2; i++) {
                if (i > col3 && i < col4) {
                    colsAdjacency++;
                }
            }
        }
        int rowsAdjacency = 0;
        if( Math.abs(col1 - col4) == 1 || Math.abs(col2 - col3) == 1 ) {
            for (int i = row1; i < row2; i++) {
                if (i > row3 && i < row4) {
                    rowsAdjacency++;
                }
            }
        }
        return Math.max(colsAdjacency, rowsAdjacency);
    }

    /**
     * mathematical distance between 2 2D points
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     */
    private double distanceTo(int x1, int y1, int x2, int y2){
        return Math.sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) );
    }

    /**
     * Sets the tiles accordingly to walls or floors
     */
    public void render(Map<Position3D, GameBlock> blocks){
        for(int i = 0; i <= this.carvedWidth; i++){
            for(int j = 0; j <= this.carvedHeight; j++){
                Position3D pos = Position3D.create(this.startX + i, this.startY + j, 0);
                if(i == 0 || j == 0 || i == this.carvedWidth || j == this.carvedHeight){
                    Wall wall = new Wall();
                    //wall.area = ;
                    blocks.put(pos, wall);
                }else{
                    blocks.put(pos, new Floor());
                }
            }
        }
        this.rendered = true;
    }

    /**
     * Sets the RNG of this room
     * @param rng
     */
    public void setRNG(Random rng){
        this.roomRNG = rng;
    }

    /**
     * Sets the RNG of this room
     * @param seed
     */
    public void setRNG(long seed){
        this.roomRNG = new Random();
        this.roomRNG.setSeed(seed);
    }

    /**
     * Returns a random position inside the room
     * @return
     */
    public Position3D getRandomPosition(){
        int randX = this.roomRNG.nextInt(this.carvedWidth - 1) + this.startX;
        int randY = this.roomRNG.nextInt(this.carvedHeight - 1) + this.startY;
        return Position3D.create(randX, randY, 0);
    }

    /**
     * Spawns Enemies
     */
    public void spawnEnemies(BinaryDivisionAreaBuilder builder, Map<Position3D, GameBlock> blocks){
        //Do not spawn in first and last rooms
        if(this.start || this.end)
            return;

        if(this.bossRoom){
            Position3D pos = this.getRandomPosition();
            while(blocks.get(pos).getBlocksMovement()){
                pos = this.getRandomPosition();
            }
            Boss boss = new Boss();
            boss.setPowerEnhancer(builder.enhancer);
            boss.setItemDropEnhancer(builder.enhancer);
            builder.addEntity(boss, pos);
        }else{
            int enemies = this.roomRNG.nextInt(4) + 1;
            for (int i = 0; i < enemies; i++){
                Position3D pos = this.getRandomPosition();
                while(blocks.get(pos).getBlocksMovement()){
                    pos = this.getRandomPosition();
                }
                Rat rat = new Rat();
                rat.setPowerEnhancer(builder.enhancer);
                rat.setItemDropEnhancer(builder.enhancer);
                builder.addEntity(rat, pos);
            }
        }
    }
}
