package cz.cuni.gamedev.nail123.roguelike.world.worlds.BinaryDivision;

import cz.cuni.gamedev.nail123.roguelike.blocks.GameBlock;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Rat;
import cz.cuni.gamedev.nail123.roguelike.entities.items.HealthPack;
import org.hexworks.zircon.api.data.Position3D;

import java.util.*;

public class LevelArea {
    /**
     * Holds all the rooms
     */
    HashSet<Room> rooms = new HashSet<Room>();

    /**
     * Holds the randomness of the level
     */
    Random rng = null;

    /**
     * Holds the allowed width of the level
     */
    private int allowedWidth = 60;

    /**
     * Holds the allowed height of the level
     */
    private int allowedHeight = 40;

    /**
     * Temporarily holds the rooms which could be visited for room connections verification step
     */
    private HashMap<Integer, Room> visited;

    /**
     * Holds the starting room from which we go and explore the level
     */
    private Room root;

    /**
     * Constructor
     * @param rng
     */
    public LevelArea(Random rng){
        this.rng = rng;
    }

    /**
     * Constructor
     * @param seed
     */
    public LevelArea(long seed){
        this.rng = new Random();
        this.rng.setSeed(seed);
    }

    /**
     * Sets size of the level
     * @param width
     * @param height
     */
    public void setSize(int width, int height){
        this.allowedWidth = width - 1;
        this.allowedHeight = height - 1;
    }

    /**
     * Generates rooms by splitting them vertically or horizontally
     * @throws Exception
     */
    public void generateRooms() throws Exception {
        root = new Room(0, 0, this.allowedWidth, this.allowedHeight);

        root.setStart(true);
        root.setEnd(true);
        root.setRNG(this.rng.nextLong());
        rooms.add(root);

        // Discarded recursive call in favour of continuous verifications of possible splitting
        // a bit worse efficiency
        Room roomToSplit = null;
        while((roomToSplit = hasRoomToSplit()) != null){
            Room new_room = roomToSplit.randomSplit();
            if(new_room != null) {
                new_room.setRNG(this.rng.nextLong());
                this.rooms.add(new_room);
            }
        }
    }

    /**
     * Renders the rooms
     * @param blocks
     */
    public void renderRooms(Map<Position3D, GameBlock> blocks){
        for (Room room: this.rooms) {
            room.render(blocks);
        }
    }

    /**
     * Goes through the rooms and carves them
     */
    public void carveRooms(){
        for (Room room: this.rooms) {
            room.carveRoom();
        }
    }

    /**
     * Goes through each room, and sets the rooms that are adjacent as neighbours
     */
    public void setRoomNeighbours(){
        for (Room thisRoom: this.rooms) {
            for (Room thatRoom: this.rooms) {
                if(thisRoom != thatRoom && !thisRoom.neighbours.contains(thatRoom) && !thatRoom.neighbours.contains(thisRoom)){
                    if(thisRoom.isAdjacentTo(thatRoom) > 2){
                        thisRoom.neighbours.add(thatRoom);
                        thatRoom.neighbours.add(thisRoom);
                    }
                }
            }
        }
    }

    /**
     * Connects rooms to their neighbours
     * @throws Exception
     */
    public void connectRooms() throws Exception{
        for (Room room: this.rooms) {
            int connectedNeighbours = 0;
            for (Room neighbour: room.neighbours) {
                if(room.connectedRooms.contains(neighbour)){
                    connectedNeighbours++;
                }
            }
            if(room.neighbours.size() - connectedNeighbours > 0){
                // connect only a portion of remaining neighbours
                int numberOfConnectedNeighbours = 0;
                if(BinaryDivisionAreaBuilder.CONNECT_ALL_NEIGHBOURS){
                    numberOfConnectedNeighbours = room.neighbours.size() - connectedNeighbours;
                }else{
                    numberOfConnectedNeighbours = this.rng.nextInt(room.neighbours.size() - connectedNeighbours);
                }
                for(int i=0; i< numberOfConnectedNeighbours; i++){
                    Room neighbour = room.neighbours.get(this.rng.nextInt(room.neighbours.size()));
                    while(room.connectedRooms.contains(neighbour)){
                        neighbour = room.neighbours.get(this.rng.nextInt(room.neighbours.size()));
                    }
                    room.connectedRooms.add(neighbour);
                    neighbour.connectedRooms.add(room);
                }
            }else{
                //throw new Exception("More connected neighbours than actual neighbours or no neighbours at all");
            }
        }
    }

    /**
     * Starts rendering tunnels between rooms
     * @param blocks
     * @throws Exception
     */
    public void renderTunnels(Map<Position3D, GameBlock> blocks) throws Exception {
        HashSet localVisited = new HashSet<>();
        Iterator<Room> it = this.rooms.iterator();
        Room currentRoom;
        while(it.hasNext()){
            currentRoom = it.next();
            for (Room connection: currentRoom.connectedRooms) {
                if(!localVisited.contains(connection)){
                    currentRoom.drawTunnelBetween(connection, blocks);
                }
            }
            localVisited.add(currentRoom);
        }
    }

    /**
     * Returns the start room
     * @return
     */
    public Room getStart(){
        for (Room room: this.rooms) {
            if(room.start){
                return room;
            }
        }
        return null;
    }

    /**
     * Returns the end room
     * @return
     */
    public Room getEnd(){
        for (Room room: this.rooms) {
            if(room.end){
                return room;
            }
        }
        return null;
    }

    /**
     * Prints the stats for each room
     */
    public void printStats(){
        System.out.println("Rooms: "+Room.roomNumber);
        for (Room room: this.rooms) {
            System.out.println("Room " + room.number + " has " + room.connectedRooms.size() + " connections and " + room.neighbours.size() + " neighbours");
        }
    }

    /**
     * Returns if there are any leftover rooms to be split
     * @return
     */
    private Room hasRoomToSplit(){
        //if(rooms.size() > 2 )
        //    return null;
        for (Room room: rooms) {
            if(!room.splitEnded){
                return room;
            }
        }
        return null;
    }

    /**
     * Verify if any rooms are unreachable if we start from the root
     * @return
     */
    private boolean roomsUnreachable(){
        this.visited = new HashMap<Integer, Room>();
        Room current_room = this.root; //start room
        this.visited.put(current_room.number, current_room); // assumed we have at least 1 room
        for (Room connection: current_room.connectedRooms) {
            this.visited.put(connection.number, connection); //add all connections of the first room
        }
        // go through each already existent connection
        int index = 0;
        while(index < this.visited.size() - 1){
            index++;
            // go to the next room
            current_room = this.visited.get(this.visited.keySet().toArray()[index]);
            //go through each connection of the new room
            for (Room room: current_room.connectedRooms) {
                // check if it is already visited
                if(!this.visited.containsKey(room.number)) {
                    // if it wasn't visited, but we can visit, add it to visits
                    this.visited.put(room.number, room);
                }
            }
        }
        // verify if the visited array has the same size as the whole rooms (aka all rooms are visitable)
        return this.visited.size() != this.rooms.size();
    }

    /**
     * Connect the rooms which couldn't be visited
     */
    public void fixRoomsUnreachable(){
        if(this.roomsUnreachable()){
            // Until the sizes of the visited and the rooms sets are equal
            while(this.visited.size() != this.rooms.size()){
                // Get all rooms
                for (Room room: this.rooms) {
                    // Which couldn't be visited
                    if(!this.visited.containsKey(room.number)){
                        // And go through the neighbours
                        for (Room neighbour: room.neighbours) {
                            // And connect to the neighbours which could be visited
                            if(this.visited.containsKey(neighbour.number)){
                                if(room.connectedRooms.contains(neighbour)){
                                    // It is possible we connected this part of the level which was previously unreachable
                                    this.visited.put(room.number, room);
                                    continue;
                                }
                                //But if we didn't, it is about time to connect it
                                room.connectedRooms.add(neighbour);
                                neighbour.connectedRooms.add(room);
                                this.visited.put(room.number, room);
                                break; //Break so that we connect only 1 such neighbour
                            }
                        }
                    }
                }
            }
        }
    }

    public void placeEnemies(BinaryDivisionAreaBuilder builder, Map<Position3D, GameBlock> blocks){
        //Make x boss rooms:
        int bosses = this.rng.nextInt(3) + 1; //at least 1 boss, and at most 3
        for (Room room: this.rooms) {
            if(bosses == 0)
                break;
            //Make random boss rooms
            if(!room.end && !room.start){
                if(this.rng.nextBoolean()) {
                    room.bossRoom = true;
                    bosses--;
                }
            }

        }
        for (Room room: this.rooms) {
            room.spawnEnemies(builder, blocks);
            //Spawn healthpack
            if(this.rng.nextBoolean()){
                Position3D pos = room.getRandomPosition();
                while(blocks.get(pos).getBlocksMovement()){
                    pos = room.getRandomPosition();
                }
                builder.addEntity(new HealthPack(4), pos);
            }
        }
    }
}
